import construct

from photon.construct_utils import *
from enum import IntEnum

__ALL__ = ['PhotonHeader']

# https://github.com/mazurwiktor/albion-online-addons/blob/master/src/photon_decode/layout.rs

PhotonHeader = construct.Struct(
    'peer_id'       / i16,
    'crc_enabled'   / bool_,
    'command_count' / u8,
    'timestamp'     / u32,
    'challenge'     / u32
)

PhotonReliableCommand = construct.Struct(
    'channel_id' / u8,
    'flags' / u8,
    'reserved' / u8,
    'msg_len' / u32,
    'reliable_sequence_number' / u32,
)

PhotonUnreliableCommand = construct.Struct(
    'reliable_command' / PhotonReliableCommand,
    'unknown' / u32
)

PhotonReliableFragment = construct.Struct(
    'reliable_command' / PhotonReliableCommand,
    'sequence_number' / u32,
    'fragment_count' / u32,
    'fragment_number' / u32,
    'total_length' / u32,
    'operation_length' / u32,
    'payload' / construct.Bytes(this.total_length) # TODO: Unsure
)

PhotonByte = u8
PhotonShort = i16
PhotonDouble = double
PhotonFloat = f16
PhotonInteger = i32
PhotonLong = i64
PhotonBool = bool_
PhotonString = construct.PascalString(i16, 'utf-8')

# Protocol16.Deserialize()
class EPhotonType(IntEnum):
    NULL         = 0
    ALSO_NULL    = 42
    DICTIONARY   = 68
    STRING_ARRAY = 97
    BYTE         = 98
    CUSTOM       = 99
    DOUBLE       = 100
    EVENT_DATA   = 101
    FLOAT        = 102
    HASHTABLE    = 104
    INTEGER      = 105
    SHORT        = 107
    LONG         = 108
    INT_ARRAY    = 110
    BOOLEAN      = 111
    OP_RESPONSE  = 112
    OP_REQUEST   = 113
    STRING       = 115
    BYTE_ARRAY   = 120
    ARRAY        = 121
    OBJECT_ARRAY = 122

PhotonValueSwitch = lambda typ: construct.Switch(typ, {
    EPhotonType.NULL:         construct.Const(b''),
    EPhotonType.ALSO_NULL:    construct.Const(b''),
    EPhotonType.DICTIONARY:   PhotonDictionary,
    EPhotonType.STRING_ARRAY: PhotonStringArray,
    EPhotonType.BYTE:         u8,
    EPhotonType.CUSTOM:       PhotonCustomType,
    EPhotonType.DOUBLE:       double,
    EPhotonType.EVENT_DATA:   PhotonEventData,
    EPhotonType.FLOAT:        f16,
    EPhotonType.HASHTABLE:    PhotonHashTable,
    EPhotonType.INTEGER:      i32,
    EPhotonType.SHORT:        i16,
    EPhotonType.LONG:         i64,
    EPhotonType.INT_ARRAY:    PhotonIntArray,
    EPhotonType.BOOLEAN:      bool_,
    EPhotonType.OP_RESPONSE:  PhotonOperationResponse,
    EPhotonType.OP_REQUEST:   PhotonOperationRequest,
    EPhotonType.STRING:       PhotonString,
    EPhotonType.BYTE_ARRAY:   PhotonByteArray,
    EPhotonType.ARRAY:        PhotonArray,
    EPhotonType.OBJECT_ARRAY: PhotonObjectArray
})

PhotonValue = construct.Struct(
    'type' / construct.Enum(u8, EPhotonType),
    'value' / PhotonValueSwitch(this.type)
)
PhotonParameterKVP = construct.Struct(
    'key' / u8,
    'value' / PhotonValue
)
PhotonParameters = construct.Struct(
    'size' / construct.Rebuild(i16, len_(this.pairs)),
    'pairs' / PhotonParameterKVP
)

PhotonEventData = construct.Struct(
    'code' / u8,
    'parameters' / PhotonParameters
)

# Protocol16.DeserializeDictionary
PhotonDictionaryEntry = construct.Struct(
    'key' / PhotonValueSwitch(this._.key_type),
    'value' / PhotonValueSwitch(this._.value_type),
)

# Protocol16.DeserializeDictionary
PhotonDictionary = construct.Struct(
    'key_type' / construct.Enum(u8, EPhotonType),
    'value_type' / construct.Enum(u8, EPhotonType),
    'size' / construct.Rebuild(i16, len_(this.entries)),
    'entries' / PhotonDictionaryEntry[this.size]
)

PhotonStringArray = construct.Struct(
    'size' / construct.Rebuild(i16, len_(this.entries)),
    'entries' / construct.PascalString(i16)[this.size],
)

PhotonCustomType = construct.Struct(
    'type' / u8,
    'length' / construct.Rebuild(i16, len_(this.raw)),
    'raw' / construct.Byte[this.length]
)

PhotonHashTableEntry = construct.Struct(
    'key' / PhotonValue,
    'value' / PhotonValue
)
PhotonHashTable = construct.Struct(
    'size' / construct.Rebuild(i16, len_(this.entries)),
    'entries' / PhotonHashTableEntry[this.size]
)

PhotonIntArray = construct.Struct(
    'size' / construct.Rebuild(i32, len_(this.elements)),
    'elements' / i32[this.size]
)

PhotonByteArray = construct.Struct(
    'size' / construct.Rebuild(i32, len_(this.elements)),
    'elements' / u8[this.size]
)

PhotonArray = construct.Struct(
    'size' / construct.Rebuild(i16, len_(this.elements)),
    'type' / u8,
    'entries' / PhotonValueSwitch(this.type)[this.size]
)

PhotonObjectArray = construct.Struct(
    'size' / construct.Rebuild(i16, len_(this.elements)),
    'entries' / PhotonValue[this.size]
)



PhotonOperationResponse = construct.Struct(
    'operation_code' / u8,
    'return_code' / i16,
    'debug_message' / PhotonValue, # String but has a type prefix.
    'parameters' / PhotonParameters
)
PhotonOperationRequest = construct.Struct(
    'operation_code' / u8,
    'parameters' / PhotonParameters
)
