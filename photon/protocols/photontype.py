import struct
from typing import Type, TypeVar, Generic, BinaryIO, Optional, Tuple, Dict, Any

__ALL__ = ['PhotonTypes']

T = TypeVar('T')
class BasePhotonType(Generic[T]):
    def __init__(self, name: str, pytype: Type) -> None:
        self.name: str = name
        self.pytype: Type = pytype
    def unpack(self, data: bytes, start: int = 0) -> Tuple[int, Optional[T]]:
        return 0, None
    def pack(self, value: T) -> bytes:
        return b''
    def calcsize(self) -> int:
        return -1
    def unpackFrom(self, f: BinaryIO) -> Optional[T]:
        return None

class SimplePhotonType(BasePhotonType[T]):
    def __init__(self, name: str, fmt: str, size: int, pytype: Type) -> None:
        super().__init__(name, pytype)
        self.format: str = fmt
        self.size: int = size
    def unpack(self, data: bytes, start: int = 0) -> Tuple[int, Optional[T]]:
        sz: int = self.calcsize()
        if (len(data)-start) < sz:
            return None
        return struct.unpack(self.format, data[start:start+sz])[0]
    def pack(self, value: T) -> bytes:
        return struct.pack(self.format, value)
    def calcsize(self) -> int:
        return self.size if self.size > 0 else struct.calcsize(self.format)
    def unpackFrom(self, f: BinaryIO) -> Optional[T]:
        sz: int = self.calcsize()
        data: bytes = f.read(sz)
        if len(data) < sz:
            return None
        return struct.unpack(self.format, data)[0]

class EventData(object):
    def __init__(self) -> None:
        super().__init__()
        self.code: int = 0
        self.parameters: Dict[int, Any] = {}
        self.senderKey: int = 254
        self.sender: int = -1
        self.customDataKey: int = 245
        self.customData: int = -1
