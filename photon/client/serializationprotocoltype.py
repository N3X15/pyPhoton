from enum import IntEnum, auto
class ESerializationProtocol(IntEnum):
    GpBinaryV16 = auto()
    GpBinaryV18 = auto()
