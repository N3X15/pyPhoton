from queue import Queue
from typing import Dict, List

from photon.client.commands.base import BaseCommand

from logging import getLogger
log = getLogger(__name__)

class ReliableSubChannel(object):
    def __init__(self, channel: 'Channel') -> None:
        self.channel = channel
        self.incoming: Dict[int, BaseCommand] = {}
        self.outgoing: Queue[BaseCommand] = Queue()
        self.incoming_sequence_number: int = 0
        self.outgoing_sequence_number: int = 0

    def clear(self) -> None:
        self.incoming.clear()
        self.outgoing.clear()

    def enqueueCommand(self, cmd: BaseCommand) -> None:
        if cmd.sequence_number == 0:
            self.outgoing_sequence_number += 1
            cmd.sequence_number = self.outgoing_sequence_number
        self.outgoing.add(cmd)

class UnreliableSubChannel(object):
    pass

class Channel(object):
    def __init__(self, cid: int) -> None:
        self.channel_id: int = cid

        self.reliable = ReliableSubChannel(self)
        self.unreliable = UnreliableSubChannel(self)

    def enqueueCommand(self, cmd) -> None:
        if cmd.isReliable:
            self.reliable.enqueueCommand(cmd)
        else:
            self.unreliable.enqueueCommand(cmd)
