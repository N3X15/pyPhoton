import construct
from photon.construct_utils import i32

from photon.client.commands.base import BaseCommand, ECommandType, ECommandFlags

#commandType = 1, sz=20, len=8
PhotonAckPayload = construct.Struct(
    'acked_command_sequence_number' / i32,
    'sent_time' / i32
)

class AckCommand(BaseCommand):
    def __init__(self, acked_command_sequence_number: int, sent_time: int) -> None:
        super().__init__(ECommandType.ACK, b'', ECommandFlags.NONE)
        self.acked_command_sequence_number: int = acked_command_sequence_number
        self.sent_time: int = sent_time

    def getExpectedPayloadSize(self) -> int:
        return 8

    def buildPayload(self) -> None:
        return PhotonAckPayload.build({
            'acked_command_sequence_number': self.acked_command_sequence_number,
            'sent_time': self.sent_time
        })

    def parsePayload(self, data: bytes) -> construct.Container:
        payload = PhotonAckPayload.parse(data)
        self.acked_command_sequence_number = payload.acked_command_sequence_number
        self.sent_time = payload.sent_time
