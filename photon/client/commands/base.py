from typing import Optional, Tuple
from enum import IntEnum, IntFlags

import construct
from photon.construct_utils import u8, i32


PhotonCommandHeader = construct.Struct(
    'type' / construct.Enum(u8, ECommandType),
    'channel_id' / u8,
    'flags' / construct.FlagsEnum(u8, ECommandFlags),
    'reserved' / u8 * 'Used when the server returns an error',
    'expected_size' / i32,
    'sequence_number' / i32
)

class ECommandType(IntEnum):
    NONE = 0
    ACK = 1
    CONNECT = 2
    VERIFYCONNECT = 3
    DISCONNECT = 4
    PING = 5
    SENDRELIABLE = 6
    SENDUNRELIABLE = 7
    SENDFRAGMENT = 8
    SENDUNSEQUENCED = 11
    SEND_EG_SERVERTIME = 12
    SEND_EG_SEND_UNRELIABLE_PROCESSED = 13
    SEND_EG_SEND_RELIABLE_UNSEQUENCED = 14
    SEND_EG_SEND_FRAGMENT_UNSEQUENCED = 15
    SEND_EG_SEND_FRAGMENT_UNSEQUENCED = 15
    SEND_EG_ACK_UNSEQUENCED = 16

class ECommandFlags(IntFlags):
    NONE = 0

    RELIABLE = 1
    UNSEQUENCED = 2

class BaseCommand(object):
    def __init__(self, commandType: Optional[ECommandType] = None, payload: Optional[bytes] = None, channelID: Optional[int] = None) -> None:
        self.type: ECommandType = commandType or ECommandType(0)
        self.flags: ECommandFlags = ECommandFlags(0)
        self.channelID: int = channelID or 0
        self.reservedByte: int = 4
        self.payload: bytes = payload

        self.sequence_number: int = 0

        self._serialized: bytes = b''

    def _build(self) -> None:
        self._serialized = self.buildHeader()
        self._serialized += self.buildPayload()
        expected_size = self.getExpectedSize()
        actual_size = len(self._serialized)
        assert actual_size == expected_size, f'Command {self} was expected to be {expected_size}B, was actually {actual_size}B'

    def getExpectedSize(self) -> int:
        return self.getExpectedHeaderSize()+self.getExpectedPayloadSize()

    def getExpectedHeaderSize(self) -> int:
        return 12
    def getExpectedPayloadSize(self) -> int:
        return 0

    def buildHeader(self) -> bytes:
        return PhotonCommandHeader.build(
            type=self.type.value,
            channel_id=self.channelID,
            flags=self.flags.value,
            reserved=self.reservedByte,
            expected_size=self.getExpectedSize(),
            sequence_number=self.sequence_number
        )

    def buildPayload(self) -> bytes:
        return b''

    def parse(self, data: bytes) -> Tuple[construct.Container, construct.Container]:
        hsz: int = self.getExpectedHeaderSize()
        hdrParsed = self.parseHeader(data[0:hsz])
        psz: int = self.getExpectedPayloadSize()
        pldParsed = self.parsePayload(data[hsz:hsz+psz])
        return (hdrParsed, pldParsed)

    def parseHeader(self, data: bytes) -> construct.Container:
        return PhotonCommandHeader.parse(data)

    def parsePayload(self, data: bytes) -> construct.Container:
        return None
