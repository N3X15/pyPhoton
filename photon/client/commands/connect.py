import construct
from photon.construct_utils import i16, u8

from photon.client.commands.base import BaseCommand, ECommandType, ECommandFlags

#commandType = 2, sz=44, len=32
# FIXME: If anyone can work out what the hell all the undefined consts are, please feel free.
# They don't seem to change and I didn't find them in the server SDK, so maybe versioning or SDK data?
PhotonConnectPayload = construct.Struct(
    construct.Const(bytes([
        0, # 0
        0  # 1
    ])),
    'mtu' / i16, # idx 2-3, value=1200 by default
    construct.Const(bytes([
        0, #4
        0, #5
        128, #6
        0, #7
        0, #8 (assumed)
        0, #9 (assumed)
        0 #10 (assumed)
    ])), # Unknown
    'channel_count' / u8, # 11
    construct.Const(bytes([
        0, #12 (assumed)
        0, #13 (assumed)
        0, #14 (assumed)
        0, #15 (specified, probably int32 of previous 4 bytes)
        0, #16 (assumed)
        0, #17 (assumed)
        0, #18 (assumed)
        0, #19 (specified, probably int32 of previous 4 bytes)
        0, #20 (assumed)
        0, #21 (assumed)
        19, #22
        136, #23 - Maybe another int32 idk
        0, # 24 (assumed)
        0, # 25 (assumed)
        0, # 26 (assumed)
        2, # 27 (Specified, maybe int32?)
        0, # 28 (assumed)
        0, # 29 (assumed)
        0, # 30 (assumed)
        2, # 31 (Specified, maybe int32?)
    ]))
)

class ConnectCommand(BaseCommand):
    def __init__(self, mtu: int=1200, channel_count: int = 2) -> None:
        super().__init__(ECommandType.CONNECT, b'', ECommandFlags.NONE)
        self.mtu: int = mtu
        self.channel_count: int = channel_count

    def getExpectedPayloadSize(self) -> int:
        return 32

    def buildPayload(self) -> None:
        return PhotonConnectPayload.build(
            mtu=self.mtu,
            channel_count=self.channel_count,
        )

    def parsePayload(self, data: bytes) -> construct.Container:
        payload = PhotonConnectPayload.parse(data)
        self.mtu = payload.mtu
        self.channel_count = payload.channel_count
