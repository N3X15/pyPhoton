from typing import Tuple

from logging import getLogger
log = getLogger(__name__)

def _parseIPPort(addressAndPort: str) -> Tuple[str, int]:
    #  ns.exitgames.com:5058
    #  http://[2001:db8:1f70::999:de8:7648:6e8]:100/
    #  [2001:db8:1f70::999:de8:7648:6e8]:100
    addr = ''
    port = 0

    idx = addressAndPort.rindex(':')
    if idx <= 0:
        raise ValueError('addressAndPort')
    if idx != addressAndPort.index(':') and ('[' not in addressAndPort or  ']' not in addressAndPort):
        raise ValueError('addressAndPort')
    addr = addressAndPort[:idx]
    port = int(addressAndPort[idx:])
    return addr, port

class Endpoint(object):
    def __init__(self, ip: str = '', port: int = 0) -> None:
        if ip != '' and port == 0:
            ip, port = _parseIPPort(ip)
        self.ip = ip
        self.port = port

    def toTuple(self) -> Tuple[str, int]:
        return (self.ip, self.port)

    def __str__(self) -> str:
        return f'<Endpoint(ip={self.ip}, port={self.port})'
