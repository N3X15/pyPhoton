import socket
from queue import Queue
from typing import Dict, List, Optional, Any
from enum import IntEnum, auto
from ipaddress import ip_address
import dns.resolver
import random

from photon.client.commands.base import BaseCommand
from photon.client.channel import Channel
from photon.client.endpoint import Endpoint
from photon.client.commands.connect import ConnectCommand

from logging import getLogger
log = getLogger(__name__)

INIT_BYTES = bytes([
    243, # Magic used to identify valid client
    0, # FIXME: No idea what the rest of this shit is.
    1,
    6,
    1,
    4,
    0,
    5,
    7
])

class EConnectionState(IntEnum):
    DISCONNECTED             = 0
    CONNECTING               = 1
    CONNECTED                = 3
    DISCONNECTING            = 4
    ACKNOWLEDGING_DISCONNECT = 5
    ZOMBIE                   = 6

class NetController(object):
    CONTROLLERS: List['NetController'] = []
    def __init__(self, config: dict) -> None:
        self.channels: Dict[int, Channel] = {}
        self.sent: List[BaseCommand] = []
        self.acks: Queue[BaseCommand] = []

        self.CONTROLLERS += [self]

        self.connectionState: EConnectionState = EConnectionState.DISCONNECTED
        self.appID: str = ''
        self.endpoint: Endpoint = None

        self._initData: bytes = None

        self._socket = None

        self.config = config.get('network', {
            'resolver': {
                'servers': None, # Use resolv.conf or registry. If array, use these servers.
                'allow-ipv4': True,
                'allow-ipv6': False,
            }
        })

    def _resolveDNSToIP(self, dns: str) -> str:
        results = dns.resolver.query(dns, 'A')
        o: List[str] = []
        for answer in results:
            ip = ip_address(answer.to_text())
            if ip.version == 6 and self.config['resolver'].get('allow-ipv6', False):
                o += [str(ip)]
            if ip.version == 4 and self.config['resolver'].get('allow-ipv4', True):
                o += [str(ip)]
        return random.choice(o)

    def connect(self, endpoint: Endpoint, appID: Optional[str] = None, custom: Optional[Any] = None) -> bool:
        if appID is None:
            appID = 'LoadBalancing'

        if self.connectionState != EConnectionState.DISCONNECTED:
            raise False

        self.endpoint = endpoint
        self.appID = appID

        # trim to 32 bytes, then justify left, padding with \x00
        # Replaces a loop.
        self._initData = (INIT_BYTES + appID.encode('utf-8'))[:32].ljust(32, b'\x00')

        self._socket = socket.socket(socket.AF_INET, # IP
                                     socket.SOCK_DGRAM) # UDP

        if not self._socket:
            log.critical('Failed to open UDP socket')
            return False

        self.connectionState = EConnectionState.CONNECTING
        self.sendCommand(ConnectCommand(mtu=1200, channel_count=2))

    def sendCommand(self, cmd: BaseCommand) -> None:
        channel = self.channels[cmd.channelID]
        channel.enqueueCommand(cmd)
