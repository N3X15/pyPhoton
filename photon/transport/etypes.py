from enum import IntEnum
class ETransportType(IntEnum):
    UDP             = 0
    TCP             = 1
    WEBSOCKET       = 4
    WEBSOCKETSECURE = 5
