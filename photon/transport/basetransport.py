from abc import ABC, abstractmethod

class BaseTransport(ABC):
    def __init__(self, peerbase) -> None:
        super().__init__()
        self.peerbase: IPhoton
