import construct
import struct
import sys

from typing import List

__ALL__ = [
    'bool_',
    'double',
    'f16',
    'i16',
    'i32',
    'i64',
    'len_',
    'this',
    'u16',
    'u32',
    'u8',
]

class FixedFormatField(construct.Construct):
    '''
    Modified FormatField to work around construct#901

    ALL 3.9.1 formats are now accessible. (except x)
    '''
    # OLD: FORMATS: List[str] = list('fdBHLQbhlqe')
    VALID_FORMATS: List[str] = list('cbB?hHiIlLqQnNefdspP')
    SIGNED_FORMATS: List[str] = list('bhilqefd')

    def __init__(self, endianity: str, format: str) -> None:
        if endianity not in list("=<>"):
            raise construct.FormatFieldError("endianity must be like: = < >", endianity)

        if format not in self.VALID_FORMATS:
            raise construct.FormatFieldError("format must be one of: %s" % (' '.join(self.VALID_FORMATS)), format)

        super().__init__()
        self.fmtstr = endianity+format
        self.length = struct.calcsize(endianity+format)
        self.packer = struct.Struct(endianity+format)

    def _parse(self, stream, context, path):
        data = construct.stream_read(stream, self.length, path)
        try:
            return self.packer.unpack(data)[0]
        except Exception:
            raise construct.FormatFieldError("struct %r error during parsing" % self.fmtstr, path=path)

    def _build(self, obj, stream, context, path):
        try:
            data = self.packer.pack(obj)
        except Exception:
            raise construct.FormatFieldError("struct %r error during building, given value %r" % (self.fmtstr, obj), path=path)
        construct.stream_write(stream, data, self.length, path)
        return obj

    def _sizeof(self, context, path):
        return self.length

    def _emitparse(self, code):
        fname = "formatfield_%s" % code.allocateId()
        code.append("%s = struct.Struct(%r)" % (fname, self.fmtstr, ))
        return "%s.unpack(read_bytes(io, %s))[0]" % (fname, self.length)

    def _emitprimitivetype(self, ksy, bitwise):
        endianity,format = self.fmtstr
        signed = format.islower()
        swapped = (endianity == "<") or (endianity == "=" and sys.byteorder == "little")
        if format in "?":
            assert not bitwise
            return 'b%s%s' % (self.length, "le" if swapped else "be", )
        if format in "efd":
            assert not bitwise
            return "f%s%s" % (self.length, "le" if swapped else "be", )
        if format in "bhlqBHLQ":
            if bitwise:
                assert not signed
                assert not swapped
                return "b%s" % (8*self.length, )
            else:
                return "%s%s%s" % ("s" if signed else "u", self.length, "le" if swapped else "be", )

bool_ = FixedFormatField('>', '?') # See https://github.com/construct/construct/issues/901
double = construct.FormatField('>', 'd')
f16 = construct.Float16b
i16 = construct.Int16sb
i32 = construct.Int32sb
i64 = construct.Int64sb
u16 = construct.Int16ub
u32 = construct.Int32ub
u8 = construct.Int8ub

this = construct.this
len_ = construct.len_
