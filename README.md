# pyPhoton
(Collides with a package on pypi, working name until I can come up with a better one)

[pyPhoton](https://gitlab.com/N3X15/pyPhoton) is a python-based packet inspection toolkit
for the popular [Photon](https://www.photonengine.com/en-US/Photon) game networking engine
by Exit Games.  I plan on making a full-featured pure-Python client SDK for Photon,
and am currently using it for a closed-source security tool.

## Status

pyPhoton is currently a work in progress and should not be used.

## Licensing

pyPhoton is available under the MIT Open-Source License.  See LICENSE for full details.
