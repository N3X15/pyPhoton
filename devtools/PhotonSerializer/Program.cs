﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace PhotonSerializer
{
    class Program
    {
        private static string BIN16_DIR = "tests/samples/protocols/bin16";
        private static string BIN18_DIR = "tests/samples/protocols/bin18";
        private static ExitGames.Client.Photon.Protocol16 PROTOCOL16;
        private static ExitGames.Client.Photon.Protocol18 PROTOCOL18;

        static void Main(string[] args)
        {
            var version = typeof(ExitGames.Client.Photon.Protocol).Assembly.GetName().Version;
            Console.WriteLine("Creating Photon Realtime Networking v{0} test samples...", version);
            if(!Directory.Exists(BIN16_DIR))
                Directory.CreateDirectory(BIN16_DIR);
            if (!Directory.Exists(BIN18_DIR))
                Directory.CreateDirectory(BIN18_DIR);
            
            PROTOCOL16 = new ExitGames.Client.Photon.Protocol16();
            PROTOCOL18 = new ExitGames.Client.Photon.Protocol18();

            serialize(new int[5] { 1, 2, 3, 4, 5 }, "array.bin");
            serialize(true, "boolean.bin");
            serialize((byte)95, "byte.bin");
            serialize(new byte[5] { 1, 2, 3, 4, 5 }, "bytearray.bin");
            var dict = new Dictionary<byte, int>() {
                {0, 0},
                {1, 1},
                {2, 2},
                {3, 3},
                {4, 4},
            };
            serialize(dict, "dictionary.bin");
            serialize(1.0000005d, "double.bin");
            serialize(0.2f, "float.bin");
            var hashTable = new Hashtable(5);
            hashTable["string"] = "abcdefg123";
            hashTable["int"] = 1;
            hashTable["float"] = 0.1f;
            hashTable["null"] = null;
            hashTable[1] = 0;
            serialize(hashTable, "hashtable.bin");
            serialize(new int[5] { 1, 2, 3, 4, 5 }, "intarray.bin");
            serialize(1, "integer.bin");
            serialize(-1234567890L, "long.bin");
            serialize("abcdefg12345", "string.bin");
            serialize((short)-256, "short.bin");
        }

        private static void serialize(object obj, string basename)
        {
            serializeForProtocol(PROTOCOL16, obj, Path.Combine(BIN16_DIR, basename));
            serializeForProtocol(PROTOCOL18, obj, Path.Combine(BIN18_DIR, basename));
        }

        private static void serializeForProtocol(ExitGames.Client.Photon.IProtocol pc16, object obj, string filename)
        {
            long written = 0;
            using (var stream = File.OpenWrite(filename))
            {
                var buffered = new ExitGames.Client.Photon.StreamBuffer();
                pc16.Serialize(buffered, obj, false);
                stream.Write(buffered.ToArray());
                written = stream.Position;
            }
            Console.WriteLine("Wrote {0}B to {1}", written, filename);
        }
    }
}
